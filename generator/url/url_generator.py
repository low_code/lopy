import os
from jinja2 import Environment, PackageLoader, select_autoescape

__env = Environment(
    loader=PackageLoader('generator', 'url')
)

def generate(project_path, name):
    """
    Generates the urls file.

    Parameters:
        project_path (string): The path to the project directory where the urls.py file should get generated
        name (string): The name of the project
    """
    template = __env.get_template('urls.py.j2')

    with open(os.path.join(project_path, name, 'urls.py'), 'w') as f:
        f.write(template.render(name=name))
