import os
#from generator.project import project_generator
from generator.model import model_generator
#from generator.view import view_generator
#from generator.flow import flow_generator
from generator.url import url_generator

def generate(project_path, name, models, views, process):
    """
    Generate the project and all necessary files.

    Args:
        project_path (string): The path to the directory where the project files should be generated in 
        name (string): The name of the process
        models (diagram_parser.plantuml_class.models.Class): Class diagram with the model definition
        views (diagram_parser.plantuml_wireframe.views): Wireframe diagram with the view definition
        process (diagram_parser.bpmnio_bpmn.process): Process diagram with the process description
    """
    #project_generator.generate(project_path, name)
    model_generator.generate(project_path, name, models)
    #view_generator.generate(project_path, models)
    #flow_generator.generate(project_path, models)
    url_generator.generate(project_path, name)