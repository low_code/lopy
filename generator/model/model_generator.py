import os
from jinja2 import Environment, PackageLoader, select_autoescape

__env = Environment(
    loader=PackageLoader('generator', 'model')
)

def generate(project_path, name, models):
    """
    Generates the model file.

    This generator takes a parsed class diagram as input and generates
    a single models.py file, ready to use in viewflow, out of it.

    Parameters:
        project_path (string): The path to the project directory where the models.py file should get generated
        name (string): The name of the process
        models (diagram_parser.plantuml_class.models.Class): The class diagram file containing the model definition
    """
    template = __env.get_template('models.py.j2')

    with open(os.path.join(project_path, name, 'app', 'models.py'), 'w') as f:
        f.write(template.render(models=models))
