import os
import unittest
from diagram_parser.plantuml_class import parser
from diagram_parser.plantuml_class import models

TEST_FILE = os.path.join(os.path.dirname(__file__), 'diagram.plantuml')

class Test_Parser(unittest.TestCase):

    def test_empty_class(self):
        text = """
            @startuml
            class MyClass {}
            @enduml
        """
        classes = parser.parse_string(text)
        
        self.assertEqual(len(classes), 1)
        c = classes[0]
        self.assertIsInstance(c, models.Class)
        self.assertEqual(c.name, 'MyClass')
        self.assertFalse(c.fields)
        self.assertFalse(c.methods)
        self.assertFalse(c.relations)


    def test_class_with_fields(self):
        text = """
            @startuml
            class MyClass {
                - field1 : String
                # field2 : Integer
                + field3 : Date
            }
            @enduml
        """
        classes = parser.parse_string(text)

        self.assertEqual(len(classes), 1)
        c = classes[0]
        self.assertIsInstance(c, models.Class)
        self.assertEqual(c.name, 'MyClass')
        self.assertEqual(len(c.fields), 3)
        self.assertEqual(c.fields[0].visibility, '-')
        self.assertEqual(c.fields[0].name, 'field1')
        self.assertEqual(c.fields[0].datatype, 'String')
        self.assertEqual(c.fields[1].visibility, '#')
        self.assertEqual(c.fields[1].name, 'field2')
        self.assertEqual(c.fields[1].datatype, 'Integer')
        self.assertEqual(c.fields[2].visibility, '+')
        self.assertEqual(c.fields[2].name, 'field3')
        self.assertEqual(c.fields[2].datatype, 'Date')
        self.assertFalse(c.methods)
        self.assertFalse(c.relations)

    def test_class_with_methods(self):
        text = """
            @startuml
            class MyClass {
                - void method1()
                # String method2(param1 : Integer)
                + Integer method3(param1 : Integer, param2 : String)
            }
            @enduml
        """
        classes = parser.parse_string(text)
        
        self.assertEqual(len(classes), 1)
        c = classes[0]
        self.assertIsInstance(c, models.Class)
        self.assertEqual(c.name, 'MyClass')
        self.assertFalse(c.fields)
        self.assertEqual(len(c.methods), 3)
        self.assertEqual(c.methods[0].visibility, '-')
        self.assertEqual(c.methods[0].returntype, 'void')
        self.assertEqual(c.methods[0].name, 'method1')
        self.assertFalse(c.methods[0].params)
        self.assertEqual(c.methods[1].visibility, '#')
        self.assertEqual(c.methods[1].returntype, 'String')
        self.assertEqual(c.methods[1].name, 'method2')
        self.assertEqual(len(c.methods[1].params), 1)
        self.assertEqual(c.methods[1].params[0].name, 'param1')
        self.assertEqual(c.methods[1].params[0].datatype, 'Integer')
        self.assertEqual(c.methods[2].visibility, '+')
        self.assertEqual(c.methods[2].returntype, 'Integer')
        self.assertEqual(c.methods[2].name, 'method3')
        self.assertEqual(len(c.methods[2].params), 2)
        self.assertEqual(c.methods[2].params[0].name, 'param1')
        self.assertEqual(c.methods[2].params[0].datatype, 'Integer')
        self.assertEqual(c.methods[2].params[1].name, 'param2')
        self.assertEqual(c.methods[2].params[1].datatype, 'String')
        self.assertFalse(c.relations)

    def test_class_with_fields_and_methods(self):
        text = """
            @startuml
            class MyClass {
                - field1 : String
                # field2 : Integer
                + field3 : Date
                - void method1()
                # String method2(param1 : Integer)
                + Integer method3(param1 : Integer, param2 : String)
            }
            @enduml
        """
        classes = parser.parse_string(text)
        
        self.assertEqual(len(classes), 1)
        c = classes[0]
        self.assertIsInstance(c, models.Class)
        self.assertEqual(c.name, 'MyClass')
        self.assertEqual(len(c.fields), 3)
        self.assertEqual(c.fields[0].visibility, '-')
        self.assertEqual(c.fields[0].name, 'field1')
        self.assertEqual(c.fields[0].datatype, 'String')
        self.assertEqual(c.fields[1].visibility, '#')
        self.assertEqual(c.fields[1].name, 'field2')
        self.assertEqual(c.fields[1].datatype, 'Integer')
        self.assertEqual(c.fields[2].visibility, '+')
        self.assertEqual(c.fields[2].name, 'field3')
        self.assertEqual(c.fields[2].datatype, 'Date')
        self.assertEqual(len(c.methods), 3)
        self.assertEqual(c.methods[0].visibility, '-')
        self.assertEqual(c.methods[0].returntype, 'void')
        self.assertEqual(c.methods[0].name, 'method1')
        self.assertFalse(c.methods[0].params)
        self.assertEqual(c.methods[1].visibility, '#')
        self.assertEqual(c.methods[1].returntype, 'String')
        self.assertEqual(c.methods[1].name, 'method2')
        self.assertEqual(len(c.methods[1].params), 1)
        self.assertEqual(c.methods[1].params[0].name, 'param1')
        self.assertEqual(c.methods[1].params[0].datatype, 'Integer')
        self.assertEqual(c.methods[2].visibility, '+')
        self.assertEqual(c.methods[2].returntype, 'Integer')
        self.assertEqual(c.methods[2].name, 'method3')
        self.assertEqual(len(c.methods[2].params), 2)
        self.assertEqual(c.methods[2].params[0].name, 'param1')
        self.assertEqual(c.methods[2].params[0].datatype, 'Integer')
        self.assertEqual(c.methods[2].params[1].name, 'param2')
        self.assertEqual(c.methods[2].params[1].datatype, 'String')
        self.assertFalse(c.relations)

    def test_association(self):
        text = """
            @startuml
            MyClass1 "*" --> "1" MyClass2
            @enduml
        """
        classes = parser.parse_string(text)

        self.assertEqual(len(classes), 2)
        c0 = classes[0]
        self.assertIsInstance(c0, models.Class)
        self.assertEqual(c0.name, 'MyClass1')
        self.assertFalse(c0.fields)
        self.assertFalse(c0.methods)
        self.assertEqual(len(c0.relations), 1)
        r0 = c0.relations[0]
        self.assertEqual(r0.source, 'MyClass1')
        self.assertEqual(r0.source_cardinality, '*')
        self.assertEqual(r0.type, '-->')
        self.assertEqual(r0.target_cardinality, '1')
        self.assertEqual(r0.target, 'MyClass2')

        c1 = classes[1]
        self.assertIsInstance(c1, models.Class)
        self.assertEqual(c1.name, 'MyClass2')
        self.assertFalse(c1.fields)
        self.assertFalse(c1.methods)
        self.assertFalse(c1.relations)

    
    def test_extension(self):
        text = """
            @startuml
            MyClass1 "*" --|> "1" MyClass2
            @enduml
        """
        classes = parser.parse_string(text)

        self.assertEqual(len(classes), 2)
        c0 = classes[0]
        self.assertIsInstance(c0, models.Class)
        self.assertEqual(c0.name, 'MyClass1')
        self.assertFalse(c0.fields)
        self.assertFalse(c0.methods)
        self.assertEqual(len(c0.relations), 1)
        r0 = c0.relations[0]
        self.assertEqual(r0.source, 'MyClass1')
        self.assertEqual(r0.source_cardinality, '*')
        self.assertEqual(r0.type, '--|>')
        self.assertEqual(r0.target_cardinality, '1')
        self.assertEqual(r0.target, 'MyClass2')

        c1 = classes[1]
        self.assertIsInstance(c1, models.Class)
        self.assertEqual(c1.name, 'MyClass2')
        self.assertFalse(c1.fields)
        self.assertFalse(c1.methods)
        self.assertFalse(c1.relations)
    
    def test_composition(self):
        text = """
            @startuml
            MyClass1 "*" *-- "1" MyClass2
            @enduml
        """
        classes = parser.parse_string(text)

        self.assertEqual(len(classes), 2)
        c0 = classes[0]
        self.assertIsInstance(c0, models.Class)
        self.assertEqual(c0.name, 'MyClass1')
        self.assertFalse(c0.fields)
        self.assertFalse(c0.methods)
        self.assertEqual(len(c0.relations), 1)
        r0 = c0.relations[0]
        self.assertEqual(r0.source, 'MyClass1')
        self.assertEqual(r0.source_cardinality, '*')
        self.assertEqual(r0.type, '*--')
        self.assertEqual(r0.target_cardinality, '1')
        self.assertEqual(r0.target, 'MyClass2')

        c1 = classes[1]
        self.assertIsInstance(c1, models.Class)
        self.assertEqual(c1.name, 'MyClass2')
        self.assertFalse(c1.fields)
        self.assertFalse(c1.methods)
        self.assertFalse(c1.relations)

    def test_aggregation(self):
        text = """
            @startuml
            MyClass1 "*" o-- "1" MyClass2
            @enduml
        """
        classes = parser.parse_string(text)

        self.assertEqual(len(classes), 2)
        c0 = classes[0]
        self.assertIsInstance(c0, models.Class)
        self.assertEqual(c0.name, 'MyClass1')
        self.assertFalse(c0.fields)
        self.assertFalse(c0.methods)
        self.assertEqual(len(c0.relations), 1)
        r0 = c0.relations[0]
        self.assertEqual(r0.source, 'MyClass1')
        self.assertEqual(r0.source_cardinality, '*')
        self.assertEqual(r0.type, 'o--')
        self.assertEqual(r0.target_cardinality, '1')
        self.assertEqual(r0.target, 'MyClass2')

        c1 = classes[1]
        self.assertIsInstance(c1, models.Class)
        self.assertEqual(c1.name, 'MyClass2')
        self.assertFalse(c1.fields)
        self.assertFalse(c1.methods)
        self.assertFalse(c1.relations)

    def test_complete_file(self):
        text = """
            @startuml
            class MyClass1 {}
            class MyClass2 {
                - field1 : String
                # field2 : Integer
                + field3 : Date
            }
            class MyClass3 {
                - void method1()
                # String method2(param1 : Integer)
                + Integer method3(param1 : Integer, param2 : String)
            }
            class MyClass4 {
                - field1 : String
                # field2 : Integer
                + field3 : Date
                - void method1()
                # String method2(param1 : Integer)
                + Integer method3(param1 : Integer, param2 : String)
            }
            MyClass5 "*" --> "1" MyClass6
            MyClass7 "*" --|> "1" MyClass8
            MyClass9 "5" *-- "3" MyClass10
            MyClass11 "1" o-- "*" MyClass12
            @enduml
        """
        classes = parser.parse_string(text)

        self.assertEqual(len(classes), 12)

    def test_file(self):
        classes = parser.parse_file(TEST_FILE)

        self.assertEqual(len(classes), 12)

 
if "__main__" == __name__:
    unittest.main()