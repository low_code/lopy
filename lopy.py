#!/usr/bin/env python
import os
import argparse
from diagram_parser import diagram_parser
from generator import generator

__version__ = '0.0.1'
__author__ = u'Michael Sieber'

class diagrams_dir(argparse.Action):
    """
    Custom type handler for argparse to verify a diagram directory.

    This handler will make sure the directory contains the necessary files:
        models.plantuml - A class diagram containing the model definitions
        views.plantuml - A wireframe diagram containing the user interface definitions
        process.bpmn - A BPMN diagram containting the process description
    """
    def __call__(self, parser, namespace, values, option_string=None):
        diagrams_dir = values

        # check directory access
        if not os.path.isdir(diagrams_dir):
            raise argparse.ArgumentTypeError("{0} is not a valid path".format(diagrams_dir))
        if os.access(diagrams_dir, os.R_OK):
            setattr(namespace, self.dest,diagrams_dir)
        else:
            raise argparse.ArgumentTypeError("{0} is not a readable dir".format(diagrams_dir))

        # check existence of all required diagram files
        models_diagram = os.path.join(diagrams_dir, 'models.plantuml')
        if not os.path.isfile(models_diagram) and not os.access(models_diagram, os.R_OK):
            raise argparse.ArgumentTypeError("{0} is not a readable file or does not exist".format(models_diagram))

        views_diagram = os.path.join(diagrams_dir, 'views.plantuml')
        if not os.path.isfile(views_diagram) and not os.access(views_diagram, os.R_OK):
            raise argparse.ArgumentTypeError("{0} is not a readable file or does not exist".format(views_diagram))

        process_diagram = os.path.join(diagrams_dir, 'process.bpmn')
        if not os.path.isfile(process_diagram) and not os.access(process_diagram, os.R_OK):
            raise argparse.ArgumentTypeError("{0} is not a readable file or does not exist".format(process_diagram))

class directory(argparse.Action):
    """
    Custom type handler for argparse to verify if a directory exists and is writeable.
    """
    def __call__(self, parser, namespace, values, option_string=None):
        dir = values

        # check directory access
        if not os.path.isdir(dir):
            raise argparse.ArgumentTypeError("{0} is not a valid path".format(dir))
        if os.access(dir, os.R_OK):
            setattr(namespace, self.dest,dir)
        else:
            raise argparse.ArgumentTypeError("{0} is not a readable dir".format(dir))

def get_arg_parser():
    """
    Creates a new argument parser.
    """
    parser = argparse.ArgumentParser('LoPy')
    version = '%(prog)s ' + __version__
    parser.add_argument('--version', '-v', action='version', version=version)
    parser.add_argument('--name', '-n', required=True, help="Specifies the name of the process")
    parser.add_argument('--input', '-i', action=diagrams_dir,
                        required=True,
                        help='Specifies the directory containing the diagrams')
    parser.add_argument('--output', '-o', action=directory,
                        required=True,
                        help='Specifies the directory in which the application will be generated')
    return parser

def run(name, diagram_path, project_path):
    """
    Run the parsers and code generators

    Args:
        name (string): Name of the process
        diagram_path (string): The directory containing the diagrams to parse
        project_path (string): The directory in which the project files will be generated
    """
    models, views, process = diagram_parser.parse_diagrams(diagram_path)
    generator.generate(project_path, name, models, views, process)

def main(args=None):
    """
    Main entry point of the lopy command line application.
    """

    argparser = get_arg_parser()
    args = argparser.parse_args(args)
    run(args.name, args.input, args.output)


if __name__ == '__main__':
    main()