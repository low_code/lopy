# LoPy

A low code platform written in Python

## Developer setup

For development the following setup is recommended:

* Visual Studio Code with the following plugins:
  * ms-python.python
  * jebbs.plantuml
  * vs-code-bpmn-io
* Python >= 3.8 with pylint and virtualenv installed

Start developing:
```bash
python3 -m venv env
source env/bin/activate
pip3 install -r requirements.txt
```

Run tests:
```python
python tests/alltests.py
```

## How to use the generator

1. Install Python
2. Create a new Django project (replace example with the name of the application)
```bash
# create the project directory and enter it
mkdir example
cd example

# create a virtual python environment and activate it
python3 -m venv env
source env/bin/activate

# download django libraries
pip install django django-material django-viewflow

# create the django project (name of the project is "example")
django-admin startproject example .
mkdir example/app

# start the project to generate the required files
./manage.py startapp app example/app
```
3. Run the code generator
```bash
# as name enter the name used in the django-admin startproject command
python3 lopy.py -n example -i /path/to/diagrams -o /path/to/example/
```
4. Run the application
```bash
# create the database migrations
./manage.py makemigrations app

# execute the migrations
./manage.py migrate

# create a super user for the application
./manage.py createsuperuser

# run the server
./manage.py runserver
```