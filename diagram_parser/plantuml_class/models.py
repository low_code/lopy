class Class:

    def __init__(self):
        self.name = None
        self.fields = []
        self.methods = []
        self.relations = []

class Field:

    def __init__(self):
        self.visibility = None
        self.datatype = None
        self.name = None

class Method:

    def __init__(self):
        self.visibility = None
        self.returntype = None
        self.name = None
        self.params = []

class Parameter:

    def __init__(self):
        self.datatype = None
        self.name = None

class Relation:

    def __init__(self):
        self.type = None
        self.source = None
        self.source_cardinality = None
        self.target = None
        self.target_cardinality = None