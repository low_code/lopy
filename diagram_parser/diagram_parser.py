import os
from diagram_parser.plantuml_class import parser
#from diagram_parser.plantuml_wireframe import parser
#from diagram_parser.bpmnio_bpmn import parser

def parse_diagrams(diagram_path):
    """
    Parse the diagrams in the given path
    
    Args:
        diagram_path (string): The path to the directory which contains the diagrams
    """
    models = parser.parse_file(os.path.join(diagram_path, 'models.plantuml'))
    #views = parser.WireframeDiagramParser().parse(os.path.join(diagram_path, 'views.plantuml'))
    #process = parser.BpmnDiagramParser().parse(os.path.join(diagram_path, 'process.bpmn'))
    return models, None, None